#!/usr/bin/env python
# coding: utf-8

# In[1]:





# In[72]:


import sqlite3 as sql
import pandas as pd
conn = sql.connect("Asst2.db")
try:
    #Create my database tables: Cuisines,Recipes,Dish,Restaurant and Cookbook
    conn.execute("""
    DROP TABLE IF EXISTS Cuisines
    """)
    conn.execute("""
    CREATE TABLE Cuisines (Country TEXT, Dish TEXT,PRIMARY KEY('Country'));
    """)
    conn.execute("""
    DROP TABLE IF EXISTS Recipes
    """)
    conn.execute("""
    CREATE TABLE Recipes (id INT, Dish TEXT, Native TEXT, PRIMARY KEY ('id'), 
                      FOREIGN KEY ('Native') REFERENCES Cuisine ('Country')
                      ON DELETE CASCADE 
                      ON UPDATE CASCADE)
    """)
    conn.execute("""
    DROP TABLE IF EXISTS Dish
    """)
    conn.execute("""
    CREATE TABLE Dish (Name TEXT, Country TEXT, FOREIGN KEY ('Name') REFERENCES Recipes ('Dish')
                   FOREIGN KEY ('Country') REFERENCES Cuisines ('Country')
                   ON DELETE CASCADE 
                   ON UPDATE CASCADE);
    """)
    conn.execute("""
    DROP TABLE IF EXISTS Restaurant
    """)
    conn.execute("""
    CREATE TABLE Restaurant (Name TEXT, MenuItemNo INT, 
                         FOREIGN KEY ('MenuItemNo') REFERENCES Recipes ('id')
                         ON DELETE CASCADE 
                         ON UPDATE CASCADE);
    
    """)
    conn.execute("""
    DROP TABLE IF EXISTS Cookbook
    """)
    conn.execute("""
    CREATE TABLE Cookbook (Name TEXT, RecipeName TEXT, FOREIGN KEY ('RecipeName') REFERENCES Recipes ('Dish')
                       ON DELETE CASCADE
                       ON UPDATE CASCADE);
    """)
    
    #Insert data into the tables
    conn.execute("""
    INSERT INTO Cuisines values ('India','Butter Chicken'),('Thailand','Pad Thai');
    """)
    conn.execute("""
    INSERT INTO Recipes values (1,'Butter Chicken','India'),(2,'Pad Thai','Thailand'),(3,'Paneer','India');
    """)
    conn.execute("""
    INSERT INTO Dish values ('Butter Chicken','India'),('Pad Thai','Thailand'),('Cheeseburger','USA');
    """)
    conn.execute("""
    INSERT INTO Restaurant values ('India of India','1'),('Siri Thai','2');
    """)
    conn.execute("""
    INSERT INTO Cookbook values ('Indian Cooking','Butter Chicken'),('Taste of Thai at Home','Pad Thai');
    """)
    
    #1 Recipes-Restaurant relation based on id
    cursor1=conn.execute("""
    select Recipes.Dish, Recipes.Native from 
    Recipes join Restaurant
    on Recipes.id = Restaurant.MenuItemNo;
    """)
    df1=pd.DataFrame(cursor1.fetchall(),columns=['Dish','Native'])
    print('Dish-Native from Recipes also in  Restaurants')
    print(df1)
    
    #2 Recipes-Cuisines relation based on Native country
    cursor2=conn.execute("""
    select Recipes.Dish, Recipes.id, Recipes.Native from
    Recipes join Cuisines
    on Recipes.Native = Cuisines.Country;
    """)
    df2=pd.DataFrame(cursor2.fetchall(),columns=['Dish','id','Native'])
    print('Dish-id-Native from Recipes also in Cuisines')
    print(df2)
    
    #3 Cuisines-Dish relation based on Country
    cursor3=conn.execute("""
    select Cuisines.Country from
    Cuisines join Dish
    on Cuisines.Country = Dish.Country;
    """)
    df3=pd.DataFrame(cursor3.fetchall(),columns=['Country'])
    print('Countries from Cuisines also in Dish')
    print(df3)
    
    #4 Cookbook-Recipes relation based on Dish
    cursor4=conn.execute("""
    select Cookbook.Name from 
    Cookbook join Recipes
    on Cookbook.RecipeName = Recipes.Dish;
    """)
    df4=pd.DataFrame(cursor4.fetchall(),columns=['Name'])
    print('Cookbook Name from Cookbook featuring recipes also in Recipes')
    print(df4)
    
    #5 Dish-Recipes relation based on Dish name
    cursor5=conn.execute("""
    select Dish.Name, Dish.Country from 
    Dish join Recipes 
    on Dish.Name = Recipes.Dish;
    """)
    df5=pd.DataFrame(cursor5.fetchall(),columns=['Name','Country'])
    print('Dish-Country from Dish also in Recipes')
    print(df5)
    
    conn.commit();
    
except sql.Error as e:
    print("There was an error."+str(e))
    
finally:
    print("This executed")
    conn.close()


# In[ ]:





# In[ ]:




